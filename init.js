/// <reference path="../Seneton.d.ts" />
// @ts-check
"use strict";


// create boards
var boardXDimension  = 10;
var boardsYDimension = 3;
var fieldWidth       = 100;
var fieldHeight      = 100;
var spacer           = 5;
var color            = ["light", "dark"];
var stoneRadius      = 75;

var board = Seneton.createElement({
	faces:        ["resources/board.jpg"],
	width:        boardXDimension  * fieldWidth  + (boardXDimension  + 1) * spacer,
	height:       boardsYDimension * fieldHeight + (boardsYDimension + 1) * spacer,
	shadowOffset: [10, 10],
	shadowRadius: 25,
	shadowColor:  [0, 0, 0, 0.7]
});

for(var i = 0; i < 14; i++) {
	var col = i % boardXDimension;
	var row = Math.floor(i / boardXDimension);
	Seneton.snapToGrid(Seneton.createElement({
		top:          Seneton.bounds(board).top   + spacer + row * (spacer + fieldHeight),
		right:        Seneton.bounds(board).right - spacer - col * (spacer + fieldWidth ),
		faces:        ["resources/" + color[(i+1) %2 ]+ "_stone.png"],
		width:        stoneRadius,
		height:       stoneRadius,
		grid:         ['magnet', 50, ['relativeTo', board, ['rectGridWithOffsets', 0, 0, fieldWidth + spacer, fieldHeight + spacer, 10, 3]]],
		shadowOffset: [3, 3],
		shadowColor:  [0, 0, 0, 0.3],
		shadowRadius: 1
	}));
};

// Würfel ************************************************************

Seneton.createElement({
	top:           Seneton.bounds(board).top,
	left:          Seneton.bounds(board).right + 100,
	faces:         [1, 2, 3, 4, 5].map(function(i) { return "resources/dice_" + i + ".png"; }),
	width:         608 * 0.3,
	height:        635 * 0.3,
	defaultAction: ['flipRandomAnimated']
});
