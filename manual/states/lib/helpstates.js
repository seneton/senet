//contains states objects
var states = [];
var currentState = null;
//add a state from a string to the arrays of states
function AddStateAsString(state) {
	states[states.length] = parent.PersistentState.fromJson(JSON.parse(state));
};

//create states into the array of states from a given list of states as string
function AddStatesAsStringList(list) {
	for (var i = 0; i < list.length; i++){
		AddStateAsString(list[i]);
	};
};

//stores the status of the help mode
var helpMode = false;

//creates links and icons as first element inside all divs of class example
//an div must provide an id which refers to the index of the array of states,
//and a title, which is used as name of the link
function AddExamples(){
	var divs = document.getElementsByClassName('example');
	var div = null;
	for (var i = 0; i< divs.length; i++) {
		divElem = divs[i];
		var name = divElem.title.toString();
		var id = divElem.id;
   		divElem.innerHTML = '<div onmouseover="ShowState(states['+id+'])" onmouseout="HideState()"><strong>'+name + " " +'</strong><img class="helpimg" id="img'+id+'" src="images/help_icon_red.png" alt="Spielsituation dauerhaft einblenden" onclick="ToggleHelpView(states['+id+'] ,\''+name+'\', \'img'+id+'\');"/>'+divElem.innerHTML+'</div>';
	};
};

/*
 * this function defines the behavior which is executed, whenever the icon of the link is clicked.
 * the help mode is toggled, the scource of the referred image will be changed, and other image resources will be reset.
 */
function ToggleHelpView(state, text, imgId) {
	var helpModeDiv = document.getElementById("help_status_div");
	var helpModeText = document.getElementById("help_status");
	var contentSpacerDiv = document.getElementById("content");
	var img = document.getElementById(imgId);
	if (currentState === state || state === null) {
		//set globals
		helpMode = false;
		currentState = null;
		//title help mode indicator
		helpModeDiv.style.display = 'none';
		contentSpacerDiv.style.height = "0px";
		//adapt link icon
		if (img != null) {
			img.src= "images/help_icon_red.png";
			img.alt = "Spielsituation ausblenden";
		 	img.title = img.alt;
		}
	}
	else {
		//set globals
		helpMode = true;
		currentState = state;
		//title help mode indicator
		helpModeDiv.style.display = 'block';
		helpModeText.innerHTML="Hilfemodus aktiv: "+(text === null ? "" : text);
		contentSpacerDiv.style.height = "50px";
		//adapt link icon
		if (img != null) {
		 	img.src= "images/help_icon_green.png";
			img.alt = "Spielsituation ausblenden";
		 	img.title = img.alt;
		}
	}
	RenderState(currentState);
	ResetAllButActive(img);
};

function ShowState(state) {
	if (helpMode  === false)
		RenderState(state);
};

function HideState() {
	if (helpMode  === false)
		RenderState(null);
};

function RenderState(state) {
	table.renderState = state;
	parent.Events.emit('state-changed');
};

function ResetAllButActive(activeImage) {
	var images = document.getElementsByClassName('helpimg');
	for (var i = 0; i< images.length; i++) {
		var image = images[i];
		if (activeImage === image) {
			image.alt = "Spielsituation ausblenden";
			image.title = image.alt;
		}else{
			image.src = "images/help_icon_red.png";
			image.alt = "Spielsituation dauerhaft einblenden";
			image.title = image.alt;
		}
	};
};